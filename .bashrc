HISTSIZE=1000
HISTFILESIZE=2000

case "$TERM" in
    screen) 
	color_prompt=yes
        alias ls="ls --color=auto"
	;;
    xterm-256color)
	color_prompt=yes
        alias ls="ls --color=auto"
	;;
esac

# Git prompt
source /etc/bash_completion.d/git-prompt
source /usr/share/bash-completion/completions/git
PS1='[\u@\h \W$(__git_ps1 " (%s)")] $ '

# Shortcuts
alias pomodoro='timer 1500 "Pomodoro finished." &'

# Add utility scripts to execution path
export PATH=$HOME/bin:$PATH

# Names for machines

# Make vim bindings default
export EDITOR=vi
set -o vi
