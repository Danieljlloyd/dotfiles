# Tunnels to needed applications
autossh -f -M 0 ubuntu@switchd.in -L 15672:localhost:15672 -N	# SwitchDin MQTT
autossh -f -M 0 ubuntu@devops.a.switchd.in -L 49080:localhost:49080 -N # Storm

# Start X at login
#if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
#  exec startx
#fi

source ~/.bashrc
