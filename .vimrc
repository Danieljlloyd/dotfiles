" Enter the current millenium
syntax on
"color desert

" Number lines.
set number

" Set whitespace requirements for different file types.
set ts=8 sw=8 noet
au filetype python set ts=4 sw=4 et
au filetype verilog set ts=2 sw=2 noet
au filetype systemverilog set ts=2 sw=2 noet
au filetype javascript set ts=2 sw=2 et
au filetype html set ts=2 sw=2 et nowrap
au filetype fortran set ts=8 sw=8 et
autocmd BufNewFile,BufRead *.md set filetype=markdown

" Colour line 81
set colorcolumn=81

" Fuzzy file search
set path+=**
set wildmenu

" Tag jumping
" Jump to a tag with C-] and jump back with C-t
command! MakeTags !ctags -R

" Autocomplete
set completeopt=longest,menuone
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
  \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
  \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

" File browsing (Open with :edit .)
filetype plugin on
let g:netrw_banner=0
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.='\(^\|\s\s\)\zs\.\S\+'

" Snippets
nnoremap ,html :-1read $HOME/.snippets/skeleton.html<CR>3jwf>a
nnoremap ,script :-1read $HOME/.snippets/script_tag.html<CR>4f"i
nnoremap ,swdev :-1read $HOME/.snippets/switchdin_device.py<CR>o

" Show unwanted whitespace.
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Vundle
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'

call vundle#end()
filetype plugin indent on
